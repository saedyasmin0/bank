<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Bank;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $balanceAccounts=Account::where('user_id',Auth::user()->id)->get();
        $balanceArray=[];
        foreach ($balanceAccounts as $account){
            $balanceArray[]=$account->number ." => ".$account->balance ;
        }
        return view('home',['balanceArray'=>$balanceArray]);
    }
    public function getContactUs()
    {

        $bankData=Bank::first();
        return view('layouts.about',['bankData'=>$bankData]);
    }

}
