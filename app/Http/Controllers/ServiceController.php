<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Service_type;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getService()
    {
        $serviceArray=Service_type::where('status','!=','3')->paginate(20);
        return view('service.service_type',['services'=>$serviceArray]);
    }
}
