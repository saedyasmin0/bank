@extends('layouts.app')
@section('content')

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Customers | BANK</title>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Sansita+Swashed:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
</head>
<body>

<section id="main" class="coloured-section">
    <div class="my-info text-center">
        <button class="btn btn-info" data-toggle="modal" data-target="#sendMoney">Make A Transaction</button>
        <a class="btn btn-info" href="history.html" data-toggle="modal" data-target="#transactionHistory">See All Transactions</a>
    </div>
    <div class="modal fade" id="sendMoney" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Enter Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="input-group mb-3">
                            <input type="text" id="enterSName" class="form-control" placeholder=" username"
                                   aria-label="username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">@gmail.com</span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" id="enterName" class="form-control" placeholder="username"
                                   aria-label="username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">@gmail.com</span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">₹ &nbsp;</span>
                            </div>
                            <input type="text" id="enterAmount" class="form-control" placeholder=" Enter The Amount"
                                   aria-label="Amount">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="sendMoney()" class="btn btn-success" data-dismiss="modal">Proceed</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="transactionHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Transaction History</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ol id="transaction-history-body">
                    </ol>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr class="full-table">
                    <th  scope="col"><b>Sr. No.</b></th>
                    <th scope="col"><b>Name</b></th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $key => $serviceitem)
                <tr class="table-1">
                    @if((Request::get('page')!=null))
                        <td>{{((Request::get('page')-1)* $services->perPage())+ $key+1 }}</td>
                    @else
                        <td>{{ $key+1 }}</td>
                    @endif
                        <td>{{$serviceitem->name}}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="10">
                        {!! $services->links() !!}
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</section>
<script src="{{asset('/assets/js/script.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>--}}
{{--<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js"></script>--}}
{{--<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"></script>--}}
</body>
</html>
@endsection
