<!doctype html>
<link href="{{ asset('/assets/css/bootstrap.min.css')}}" rel="stylesheet" >
<link href="{{ asset('/assets/css/app.css')}}" rel="stylesheet" >
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/jpeg" href="{{ asset('/assets/image/favicon-bank.png')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/style.css')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Banking Customer Service') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .dropbtn {
            color: skyblue;
        }



        .dropdown-content {
            display: none;
            position: absolute;
            /*background-color: skyblue;*/
            /*min-width: 160px;*/
            /*box-shadow: skyblue;*/
            /*z-index: 1;*/
        }

        .dropdown-content a {
            color: skyblue;
            /*padding: 12px 16px;*/
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {background-color: skyblue}

        .dropdown:hover .dropdown-content {
            display: block;
        }


    </style>
</head>
<body>
    <div id="app">

        <main class="py-4">
            <div id="mySidenav" class="sidenav">
                <button style="background: unset;border: unset" href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</button>
                <a href="{{route('home')}}"><img style="width: 50px;" src="{{asset('/assets/image/logo.png')}}" alt="Bank">
                </a>
                <a href="#">My Account</a>
                <a href="{{route('service')}}">Services</a>
                <a href="#">Contact</a>
                <a href="{{route('contactUs')}}">About</a>
            </div>

            <div id="main">
                <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                    <div class="container">
                        @if(\Illuminate\Support\Facades\Auth::user())

                        <span style="color:skyblue;font-size:30px;cursor:pointer" onclick="openNav()">&#9776;
</span>
                        @endif
                            <span style="color:skyblue;font-size:30px;cursor:pointer">
                                {{ config('app.name', 'Banking Customer Service') }}
                            </span>


                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav me-auto">

                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ms-auto">
                                <!-- Authentication Links -->
                                @guest
                                    @if (Route::has('login'))
                                        <li class="nav-item">
                                            <a style="color: skyblue;" class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                    @endif
                                        @if (Route::has('register'))
                                            <li class="nav-item">
                                                <a style="color: skyblue;" class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                            </li>
                                        @endif

                                @else



                                    <div class="dropdown">
{{--                                        <button class="dropbtn">{{ Auth::user()->name }}</button>--}}
{{--                                        <div class="dropdown-content">--}}
                                            <a style="color: skyblue;" class="nav-link" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                          
                            @endguest
                            </ul>
                        </div>
                    </div>
                </nav>

                @yield('content')

            </div>


        </main>
    </div>
</body>
<script src="{{ asset('/assets/js/jquery-3.6.0.min.js')}}"  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ asset('/assets/js/bootstrap.bundle.min.js')}}"  integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>


<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
    }
</script>
</html>
